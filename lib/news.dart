import 'package:flutter/material.dart';

class news extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    final screenHeight = MediaQuery.of(context).size.height;
    return SingleChildScrollView(
        child: Container(
          height: MediaQuery.of(context).orientation == Orientation.portrait
              ?screenHeight * 1.45
              :screenWidth * 2.7,
          width: screenWidth,
      decoration: BoxDecoration(
        image: DecorationImage(
            image: NetworkImage(
              "https://media.discordapp.net/attachments/807890622961680394/1070242261864042588/rm380-16.jpg?width=1014&height=676",
            ),
            fit: BoxFit.cover),
      ),
      padding: EdgeInsets.all(20),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Card(
            color: Colors.yellow.shade300,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
            child: Padding(
              padding: EdgeInsets.all(8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Image.network(
                    'https://media.discordapp.net/attachments/807890622961680394/1070250593425301534/NocticBuu.png',
                    fit: BoxFit.cover,
                  ),
                  Text('Online Learing',
                      style: Theme.of(context).textTheme.headline5),
                  SizedBox(
                    height: 8,
                  ),
                ],
              ),
            ),
          ),
          Card(
            color: Colors.yellow.shade300,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
            child: Padding(
              padding: EdgeInsets.all(8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Image.network(
                    'https://media.discordapp.net/attachments/807890622961680394/1070254862320816168/NocticBuu2.png',
                    fit: BoxFit.cover,
                  ),
                  Text('Fund', style: Theme.of(context).textTheme.headline5),
                  SizedBox(
                    height: 8,
                  ),
                ],
              ),
            ),
          ),
          Card(
            color: Colors.yellow.shade300,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
            child: Padding(
              padding: EdgeInsets.all(8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Image.network(
                    'https://media.discordapp.net/attachments/807890622961680394/1070257944786116679/NoctisBuu3.jpg?width=676&height=676',
                    fit: BoxFit.cover,
                  ),
                  Text('Online Learing',
                      style: Theme.of(context).textTheme.headline5),
                  SizedBox(
                    height: 8,
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    ));
  }
}
