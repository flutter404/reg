import 'package:flutter/material.dart';

class table extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    final screenHeight = MediaQuery.of(context).size.height;
    return SingleChildScrollView(
        child: Container(
      height: MediaQuery.of(context).orientation == Orientation.portrait
          ?screenHeight
          :screenWidth + 10,
      width: screenWidth,
      decoration: BoxDecoration(
          image: DecorationImage(
              image: NetworkImage(
                  "https://media.discordapp.net/attachments/807890622961680394/1070242261864042588/rm380-16.jpg?width=1014&height=676"),
              fit: BoxFit.cover)),
      padding: EdgeInsets.zero,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Row(
            children: [
              Column(
                children: [
                  Container(
                      color: Colors.grey.shade400,
                      child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "Name :",
                              style: TextStyle(
                                  fontSize: 22, fontWeight: FontWeight.w200),
                            ),
                            Text(
                              "Status :",
                              style: TextStyle(
                                  fontSize: 22, fontWeight: FontWeight.w200),
                            ),
                            Text(
                              "School Year :",
                              style: TextStyle(
                                  fontSize: 22, fontWeight: FontWeight.w200),
                            ),
                          ]))
                ],
              ),
              SizedBox(
                width: 5,
              ),
              Column(
                children: [
                  Container(
                      color: Colors.grey.shade200,
                      child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "Pasin Rukwarakul",
                              style: TextStyle(
                                  fontSize: 22, fontWeight: FontWeight.w200),
                            ),
                            Text(
                              "Collegian",
                              style: TextStyle(
                                  fontSize: 22, fontWeight: FontWeight.w200),
                            ),
                            Text(
                              "2565",
                              style: TextStyle(
                                  fontSize: 22, fontWeight: FontWeight.w200),
                            ),
                          ]))
                ],
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image.network(
                "https://media.discordapp.net/attachments/766152144663347210/1070280041700794450/image.png",
                width: screenWidth,
              )
            ],
          ),
          SizedBox(
            height: 10,
          ),
          Text(
            "Exam Dchedule",
            style: TextStyle(fontSize: 14, fontWeight: FontWeight.w200),
          ),
          SizedBox(
            height: 10,
          ),
          Row(
            children: [
              DataTable(columnSpacing: screenWidth <= 400
                                      ?2
                                      :7
                  , columns: [
                DataColumn(
                  label: Text("Code"),
                ),
                DataColumn(
                  label: Text("Name"),
                ),
                DataColumn(
                  label: Text("Sec"),
                ),
                DataColumn(
                  label: Text("Mid"),
                ),
                DataColumn(
                  label: Text("Final"),
                ),
              ], rows: [
                DataRow(cells: [
                  DataCell(Text("88624359\n59")),
                  DataCell(Text("Web\nProgramming")),
                  DataCell(Text("2")),
                  DataCell(Text("16/1/66\n17-20\nIF-4C02")),
                  DataCell(Text("27/3/66\n17-20\nIF-4C01")),
                ]),
                DataRow(cells: [
                  DataCell(Text("88624459\n59")),
                  DataCell(Text("OOAD")),
                  DataCell(Text("2")),
                  DataCell(Text("17/1/66\n17-20\nIF-11M208")),
                  DataCell(Text("28/3/66\n17-20\nIF-11M280")),
                ]),
                DataRow(cells: [
                  DataCell(Text("88624559\n59")),
                  DataCell(Text("Software\nTesting")),
                  DataCell(Text("2")),
                  DataCell(Text("16/1/66\n9-12 \nIF-3M210")),
                  DataCell(Text("27/3/66\n9-12 \nIF-3M210")),
                ]),
                DataRow(cells: [
                  DataCell(Text("88624359\n59")),
                  DataCell(
                      Text("Multimedia\nProgramming\nfor\nMultiplatforms")),
                  DataCell(Text("2")),
                  DataCell(Text("18/1/66\n13-16\nIF-7T01")),
                  DataCell(Text("29/3/66\n13-16\nIF-3M210")),
                ]),
                DataRow(cells: [
                  DataCell(Text("88624359\n59")),
                  DataCell(Text("Mobile\nApplication\nDevelopment")),
                  DataCell(Text("2")),
                  DataCell(Text("20/1/66\n13-16\nARR")),
                  DataCell(Text("29/3/66\n9-12\nIF-4C02")),
                ]),
                DataRow(cells: [
                  DataCell(Text("88624359\n59")),
                  DataCell(Text("NLP")),
                  DataCell(Text("2")),
                  DataCell(Text("18/1/66\n17-20\nIF-11M280")),
                  DataCell(Text("31/3/66\n09-12\nIF-11M280")),
                ]),
              ])
            ],
          )
        ],
      ),
    ));
  }
}
