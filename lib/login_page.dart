import 'package:flutter/material.dart';
import 'package:column_widget_example/main_menu.dart';
class LoginPage extends StatelessWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('REG BUU'),
      ),
      body: Container(

        decoration: BoxDecoration(
          image: DecorationImage(
            image: NetworkImage("https://media.discordapp.net/attachments/807890622961680394/1070242261864042588/rm380-16.jpg?width=1014&height=676"),fit:BoxFit.cover
          )
        ),
        child: Center(
          child: Column(
          children: [
            const Spacer(flex: 4  ),
            Image.network(
              "https://i.ibb.co/QK9cKhh/logo-buu-03.png",height: 100,
            ),

            // const SizedBox(height: .0),
            const SizedBox(width:200,child:TextField(decoration: InputDecoration(labelText: 'Username'))),
            const SizedBox(width:200,child:TextField(decoration: InputDecoration(labelText: 'Password'))),
            const Spacer(flex: 5),
            ElevatedButton(onPressed: () {
              Navigator.pushReplacement(context,
              MaterialPageRoute(builder: (context){
                return MainMenu();
                  }));
            }, child: const Text('Login')),
            const Spacer(flex: 8),
          ],
        ),
        ),
      ),
    );
  }
}
