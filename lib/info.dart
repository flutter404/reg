import 'package:flutter/material.dart';

class info extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    final screenHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      body:SingleChildScrollView(
      child:Container(
          height: MediaQuery.of(context).orientation == Orientation.portrait
              ?screenHeight * 1
              :screenWidth * 2.7,
          decoration: BoxDecoration(
            image: DecorationImage(
                image: NetworkImage(
                  "https://media.discordapp.net/attachments/807890622961680394/1070242261864042588/rm380-16.jpg?width=1014&height=676",
                ),
                fit: BoxFit.cover
            ),
          ),
          padding: EdgeInsets.all(5),
          child: Padding(
            padding: EdgeInsets.all(5),
            child: ListView(
              children: [
                Card(
                  color: Colors.grey.shade600,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  child: Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Image.network(
                          'https://media.discordapp.net/attachments/807890622961680394/1054979406097023127/IMG_20221221_113108.jpg?width=764&height=676',
                          fit: BoxFit.cover,
                        ),
                        SizedBox(
                          height: 8,
                        ),
                      ],
                    ),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      children: [
                        Container(
                          width: 118,
                          height: 30,
                          color: Colors.grey,
                          child: Text(
                            "ID",
                            style: TextStyle(
                                fontSize: 22, fontWeight: FontWeight.w200),
                          ),
                        ),
                        Container(
                          width: 118,
                          height: 30,
                          color: Colors.grey.shade200,
                          child: Text(
                            "63160207",
                            style: TextStyle(
                                fontSize: 22, fontWeight: FontWeight.w200),
                          ),
                        )
                      ],
                    ),
                    Column(
                      children: [
                        Container(
                          width: 118,
                          height: 30,
                          color: Colors.grey,
                          child: Text(
                            "AGE",
                            style: TextStyle(
                                fontSize: 22, fontWeight: FontWeight.w200),
                          ),
                        ),
                        Container(
                          width: 118,
                          height: 30,
                          color: Colors.grey.shade200,
                          child: Text(
                            "21",
                            style: TextStyle(
                                fontSize: 22, fontWeight: FontWeight.w200),
                          ),
                        )
                      ],
                    ),
                    Column(
                      children: [
                        Container(
                          width: 118,
                          height: 30,
                          color: Colors.grey,
                          child: Text(
                            "GPA",
                            style: TextStyle(
                                fontSize: 22, fontWeight: FontWeight.w200),
                          ),
                        ),
                        Container(
                          width: 118,
                          height: 30,
                          color: Colors.grey.shade200,
                          child: Text(
                            "3.45",
                            style: TextStyle(
                                fontSize: 22, fontWeight: FontWeight.w200),
                          ),
                        )
                      ],
                    ),
                  ],
                ), //id age gpa
                SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      children: [
                        Container(
                          width: 177,
                          height: 30,
                          color: Colors.grey,
                          child: Text(
                            "Date",
                            style: TextStyle(
                                fontSize: 22, fontWeight: FontWeight.w200),
                          ),
                        ),
                        Container(
                          width: 177,
                          height: 30,
                          color: Colors.grey.shade200,
                          child: Text(
                            "04 / July / 2544",
                            style: TextStyle(
                                fontSize: 22, fontWeight: FontWeight.w200),
                          ),
                        )
                      ],
                    ),
                    Column(
                      children: [
                        Container(
                          width: 177,
                          height: 30,
                          color: Colors.grey,
                          child: Text(
                            "Gender",
                            style: TextStyle(
                                fontSize: 22, fontWeight: FontWeight.w200),
                          ),
                        ),
                        Container(
                          width: 177,
                          height: 30,
                          color: Colors.grey.shade200,
                          child: Text(
                            "Male",
                            style: TextStyle(
                                fontSize: 22, fontWeight: FontWeight.w200),
                          ),
                        )
                      ],
                    ),
                  ],
                ), //date Gender
                SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      children: [
                        Container(
                          width: 177,
                          height: 30,
                          color: Colors.grey,
                          child: Text(
                            "Name",
                            style: TextStyle(
                                fontSize: 22, fontWeight: FontWeight.w200),
                          ),
                        ),
                        Container(
                          width: 177,
                          height: 30,
                          color: Colors.grey.shade200,
                          child: Text(
                            "Pasin",
                            style: TextStyle(
                                fontSize: 22, fontWeight: FontWeight.w200),
                          ),
                        )
                      ],
                    ),
                    Column(
                      children: [
                        Container(
                          width: 177,
                          height: 30,
                          color: Colors.grey,
                          child: Text(
                            "SurName",
                            style: TextStyle(
                                fontSize: 22, fontWeight: FontWeight.w200),
                          ),
                        ),
                        Container(
                          width: 177,
                          height: 30,
                          color: Colors.grey.shade200,
                          child: Text(
                            "Rukwarakul",
                            style: TextStyle(
                                fontSize: 22, fontWeight: FontWeight.w200),
                          ),
                        )
                      ],
                    ),
                  ],
                ), //Name SurName
                SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      children: [
                        Container(
                          width: 177,
                          height: 30,
                          color: Colors.grey,
                          child: Text(
                            "education level",
                            style: TextStyle(
                                fontSize: 22, fontWeight: FontWeight.w200),
                          ),
                        ),
                        Container(
                          width: 177,
                          height: 30,
                          color: Colors.grey.shade200,
                          child: Text(
                            "Bachelor's degree",
                            style: TextStyle(
                                fontSize: 22, fontWeight: FontWeight.w200),
                          ),
                        )
                      ],
                    ),
                    Column(
                      children: [
                        Container(
                          width: 177,
                          height: 30,
                          color: Colors.grey,
                          child: Text(
                            "faculty",
                            style: TextStyle(
                                fontSize: 22, fontWeight: FontWeight.w200),
                          ),
                        ),
                        Container(
                          width: 177,
                          height: 30,
                          color: Colors.grey.shade200,
                          child: Text(
                            "informatics",
                            style: TextStyle(
                                fontSize: 22, fontWeight: FontWeight.w200),
                          ),
                        )
                      ],
                    ),
                  ],
                ),//Level Degree
                SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      children: [
                        Container(
                          width: 177,
                          height: 30,
                          color: Colors.grey,
                          child: Text(
                            "CAMPUS:",
                            style: TextStyle(
                                fontSize: 22, fontWeight: FontWeight.w200),
                          ),
                        ),
                        Container(
                          width: 177,
                          height: 30,
                          color: Colors.grey.shade200,
                          child: Text(
                            "BangSean",
                            style: TextStyle(
                                fontSize: 22, fontWeight: FontWeight.w200),
                          ),
                        )
                      ],
                    ),
                    Column(
                      children: [
                        Container(
                          width: 177,
                          height: 30,
                          color: Colors.grey,
                          child: Text(
                            "GraduatedFrom",
                            style: TextStyle(
                                fontSize: 22, fontWeight: FontWeight.w200),
                          ),
                        ),
                        Container(
                          width: 177,
                          height: 30,
                          color: Colors.grey.shade200,
                          child: Text(
                            "ThaweeThaPiSek",
                            style: TextStyle(
                                fontSize: 22, fontWeight: FontWeight.w200),
                          ),
                        )
                      ],
                    ),
                  ],
                ),//Level Degree
                SizedBox(
                  height: 10,
                ),
              ],
            ),
          )),
      ));
  }
}
