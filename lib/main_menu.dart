import 'package:column_widget_example/info.dart';
import 'package:column_widget_example/login_page.dart';
import 'package:column_widget_example/news.dart';
import 'package:column_widget_example/table.dart';
import 'package:flutter/material.dart';

class MainMenu extends StatefulWidget {
  @override
  _MainMenuState createState() => _MainMenuState();
}

class _MainMenuState extends State<MainMenu> {
  Widget _currentPage = news();

  void _changePage(Widget page) {
    setState(() {
      _currentPage = page;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Drawer(
          backgroundColor: Colors.grey.shade300,
          child: ListView(
            padding: EdgeInsets.zero,
            children: [
              UserAccountsDrawerHeader(
                accountName: Text("Pasin Rukwarakul"),
                accountEmail: Text("63160207@go.buu.ac.th"),
                currentAccountPicture: CircleAvatar(
                  child: ClipOval(
                    child: Image.network(
                      "https://media.discordapp.net/attachments/807890622961680394/1054979406097023127/IMG_20221221_113108.jpg?width=764&height=676",
                      width: 90,
                      height: 90,
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ),
              ListTile(
                leading: Icon(Icons.newspaper, color: Colors.black),
                title: Text("News"),
                onTap: () {
                  _changePage(news());
                  Navigator.pop(context);
                },
              ),
              ListTile(
                leading: Icon(Icons.view_comfortable, color: Colors.black),
                title: Text("Table"),
                onTap: () {
                  _changePage(table());
                  Navigator.pop(context);
                },
              ),
              ListTile(
                leading: Icon(Icons.person, color: Colors.black),
                title: Text("Info"),
                onTap: () {
                  _changePage(info());
                  Navigator.pop(context);
                },
              ),
              ListTile(
                leading: Icon(Icons.logout, color: Colors.black),
                title: Text("Logout"),
                onTap: () {
                  Navigator.pushReplacement(context,
                      MaterialPageRoute(builder: (context) => LoginPage()));
                },
              ),
            ],
          )),
      appBar: AppBar(
        title: Text("REG BUU"),
        actions: [
          IconButton(
              icon: Icon(Icons.logout,color:Colors.black),
              onPressed: () {
                Navigator.pushReplacement(context,
                    MaterialPageRoute(builder: (context) {
                  return LoginPage();
                }));
              })
        ],
      ),
      body: _currentPage,
    );
  }
}